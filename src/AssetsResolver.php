<?php

namespace Drupal\nastorybook;

use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Asset\CssCollectionRenderer;
use Drupal\Core\Asset\JsCollectionRenderer;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Assets resolver service.
 */
class AssetsResolver {

  /**
   * Asset resolver service.
   *
   * @var \Drupal\Core\Asset\AssetResolverInterface
   */
  private $assetResolver;

  /**
   * Theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  private $themeHandler;


  /**
   * Constructs an AssetsResolver object.
   */
  public function __construct(
    AssetResolverInterface $assetResolver,
    ThemeHandlerInterface $themeHandler,
  ) {
    $this->assetResolver = $assetResolver;
    $this->themeHandler = $themeHandler;
  }

  public function getThemeLibrariesData($theme_name = '') {
    $libraries = $this->getThemeLibraries($theme_name);
    $data = [
      'css' => $this->getLibrariesStyles($libraries, TRUE),
    ];

    $css = $this->getLibrariesStyles($libraries, TRUE);
    $js = [];
    return $data;
  }

  public function getThemeLibraries($theme_name = '') {
    if (empty($theme_name)) {
      $theme_name = $this->themeHandler->getDefault();
    }
    if (!$this->themeHandler->themeExists($theme_name)) {
      return [];
    }
    $theme = $this->themeHandler->getTheme($theme_name);
    $libraries = [];
    if ($theme->base_theme) {
      $base_theme = $this->themeHandler->getTheme($theme->base_theme);
      $libraries += $base_theme->libraries ?? [];
    }
    $libraries += $theme->libraries ?? [];
    return  $libraries;
  }

  public function getLibrariesStyles($libraries, $optimize = FALSE, $already_loaded = []) {
    $assets = new AttachedAssets();
    $assets->setLibraries($libraries);
    if (!empty($already_loaded)) {
      $assets->setAlreadyLoadedLibraries($already_loaded);
    }
    $css_assets = $this->assetResolver->getCssAssets($assets, $optimize);
    $css_files = [];
    foreach ($css_assets as $item) {
      $url = Url::fromUserInput($item['data']);
      $url->setAbsolute();
      $css_files[] = $url->toString();
    }
    return $css_files;
  }

  public function getLibrariesScripts($libraries, $optimize = FALSE, $already_loaded = []) {
    $assets = new AttachedAssets();
    $assets->setLibraries($libraries);
    if (!empty($already_loaded)) {
      $assets->setAlreadyLoadedLibraries($already_loaded);
    }
    $js_assets = $this->assetResolver->getJsAssets($assets, $optimize);
    $js_files = [
      'header' => [],
      'footer' => [],
    ];
    foreach ($js_assets as $group) {
      foreach ($group as $item) {
        if ($item['type'] !== 'file') {
          continue;
        }
        $url = Url::fromUserInput($item['data']);
        $url->setAbsolute();
        $js_files[$item['scope'] ?? 'header'][] = $url->toString();
      }
    }
    return $js_files;
  }

}
