<?php

declare(strict_types=1);

namespace Drupal\nastorybook\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for NightAr StoryBook. routes.
 */
final class ServerDataController extends ControllerBase {

  /**
   * Theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The controller constructor.
   */
  public function __construct(ThemeHandlerInterface $themeHandler, FileSystemInterface $fileSystem) {
    $this->themeHandler = $themeHandler;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('theme_handler'),
      $container->get('file_system'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(Request $request) {

    $response = new CacheableJsonResponse();
    $response->setData([
      'host' => $request->getSchemeAndHttpHost(),
      'basePath' => $this->fileSystem->realpath('.'),
      'publicPath' => PublicStream::basePath(),
      'themePath' => $this->themeHandler->getTheme($this->themeHandler->getDefault())->getPath(),
      'basePaths' => PublicStream::basePath(),
    ]);

    $response->setCache([
      'max_age' => 0,
    ]);

    return $response;
  }

  public function getStories() {
    $response = new CacheableJsonResponse();
    $path = $this->themeHandler->getTheme($this->themeHandler->getDefault())->getPath();
    $path = $this->fileSystem->realpath($path);
    $map = [
      "../stories/**/*.mdx",
      "../stories/**/*.stories.@(json|yaml|yml)",
    ];
    $map[] =  $path . "/stories/**/*.mdx";
    $map[] =  $path . "/stories/**/*.stories.@(json|yaml|yml)";

    $response->setData($map);
    $response->setCache([
      'max_age' => Cache::PERMANENT,
    ]);

    return $response;
  }
}
