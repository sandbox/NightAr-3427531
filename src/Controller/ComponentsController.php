<?php

declare(strict_types=1);

namespace Drupal\nastorybook\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\nastorybook\AssetsResolver;
use Drupal\nastorybook\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for NightAr StoryBook. routes.
 */
class ComponentsController extends ControllerBase {

  /**
   * Renderer service.
   *
   * @var \Drupal\nastorybook\RendererInterface
   */
  private $renderer;

  /**
   * Assets resolver service.
   *
   * @var \Drupal\nastorybook\AssetsResolver
   */
  private $assetsResolver;

  /**
   * Theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  private $themeHandler;

  /**
   * The controller constructor.
   *
   * @param \Drupal\nastorybook\RendererInterface $renderer
   *   Renderer service.
   */
  public function __construct(
    RendererInterface $renderer,
    AssetsResolver $assetsResolver,
    ThemeHandlerInterface $themeHandler
  ) {
    $this->renderer = $renderer;
    $this->assetsResolver = $assetsResolver;
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('nastorybook.renderer'),
      $container->get('nastorybook.assets_resolver'),
      $container->get('theme_handler')
    );
  }

  /**
   * Builds the response.
   */
  public function content(Request $request, $id) {
    $args = $request->getContent();
    $args = json_decode($args, TRUE) ?? [];
    $response = new Response();
    $this->renderer->setParameters($args);
    try {
      $content = $this->renderer->renderTemplate($id);
    }
    catch (\Exception $e) {
      return $this->errorResponse($e, $response);
    }

    $response->setContent($content);
    return $response;
  }

  public function globals () {
    $response = new JsonResponse();
    $librares = $this->assetsResolver->getThemeLibraries($this->themeHandler->getDefault());
    $css = $this->assetsResolver->getLibrariesStyles($librares, TRUE);
    $js = $this->assetsResolver->getLibrariesScripts($librares, TRUE);
    $response->setData([
      'css' => $css,
      'js' => $js
    ]);

    return $response;
  }

  /**
   * @param \Exception $e
   *   Exception object.
   * @param null|Response $response
   *   Response object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function errorResponse(\Exception $e, $response = NULL) {
    if (!$response instanceof Response) {
      $response = new Response();
    }
    $response->setContent($e->getMessage());
    return $response;
  }

}
