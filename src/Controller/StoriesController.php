<?php

namespace Drupal\nastorybook\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\nastorybook\StoriesData;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StoriesController extends ControllerBase {

  /**
   * Theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Module hanlder service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Stories data servoce.
   *
   * @var \Drupal\nastorybook\StoriesData
   */
  protected $storiesData;

  /**
   * The controller constructor.
   */
  public function __construct(ThemeHandlerInterface $themeHandler, FileSystemInterface $fileSystem, ModuleHandlerInterface $moduleHandler, StoriesData $storiesData) {
    $this->themeHandler = $themeHandler;
    $this->fileSystem = $fileSystem;
    $this->moduleHandler = $moduleHandler;
    $this->storiesData = $storiesData;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('theme_handler'),
      $container->get('file_system'),
      $container->get('module_handler'),
      $container->get('nastorybook.stories_data')
    );
  }

  public function getStories() {
    $response = new CacheableJsonResponse();
    $path = $this->storiesData->getDirestories();
    $response->setData($path);
    $response->setCache([
      'max_age' => Cache::PERMANENT,
    ]);
    return $response;
  }

}
