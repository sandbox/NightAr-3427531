<?php

namespace Drupal\nastorybook;

use Drupal\Component\PhpStorage\FileStorage;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for template_manager plugins.
 */
abstract class TemplatePluginBase extends PluginBase implements TemplateInterface, ContainerFactoryPluginInterface {

  /**
   * The template ID.
   *
   * @var string
   */
  protected $Id;

  /**
   * The template parameters.
   *
   * @var array
   */
  protected $parameters;

  /**
   * Path to data.
   *
   * @var string
   */
  protected $pathData;

  /**
   * Constructs a new template plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {

    $plugin_configuration = $this->prepareConfiguration($configuration);

    parent::__construct($plugin_configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->Id;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => $this->getId(),
    ];
    $this->setData($build);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters() {
    return $this->parameters ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter($name, $default = NULL) {
    return $this->parameters[$name] ?? $default;
  }

  /**
   * Prepare and clear configurations paramenters.
   * @param array $configuration
   *   The plugin configuration.
   *
   * @return array
   *   Plugin configuration;
   */
  protected function prepareConfiguration($configuration) {
    $parameters = [];
    $plugin_configration = [];
    foreach ($configuration as $key => $value) {
      switch ($key) {
        case '_ID':
          $this->Id = $value;
          $plugin_configration['id'] = $value;
          break;

        case '_data':
          if (is_string($value)) {
            $data = $this->readStorage($value);
            $this->pathData = $value;
          }
          else {
            $data = $value;
          }
          $parameters = array_merge($data, $parameters);
          break;

        default:
          $parameters[$key] = $value;
      }
    }
    if (!empty($parameters)) {
      $this->setParameters($parameters);
    }
    return $plugin_configration;
  }

  /**
   * Set the template parameters.
   *
   * @param array $parameters
   *   The template parameters.
   */
  protected function setParameters(array $parameters) {
    $this->parameters = $parameters;
  }

  /**
   * Read storage by path.
   *
   * @param $path
   *   Path to storage.
   *
   * @return array
   *   The storage data.
   */
  protected function readStorage($path) {
    if (!is_file($path)) {
      return NULL;
    }
    $file = file_get_contents($path);
    $file_data = Json::decode($file);
    $data = [];
    if (null === $data && JSON_ERROR_NONE !== json_last_error()) {
      throw new \Exception(json_last_error_msg());
    }
    $global = $file_data['_'] ?? [];
    $template_data = $file_data[$this->getId()] ?? [];
    if (empty($global) && empty($template_data)) {
      $data = $file_data;
    }
    else {
      $data = array_merge($global, $template_data);
    }
    return $data;
  }

  /**
   * Set build data variables.
   *
   * @param array $build
   *   Build data.
   */
  protected function setData(array &$build) {
    $parameters = $this->getParameters();
    foreach ($parameters as $key => $value) {
      $build['#' . $key] = $value;
    }
  }

  /**
   * Get plugin configuration.
   *
   * @return array
   *   Configuration data.
   */
  public function getConfiguration() {
    return $this->configuration;
  }

}
