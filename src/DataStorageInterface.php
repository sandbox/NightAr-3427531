<?php

namespace Drupal\nastorybook;

/**
 * Interface for template_manager plugins.
 */
interface DataStorageInterface {

  /**
   * Get the storage file extension.
   *
   * @return string
   *   The plugin storage file extension.
   */
  public static function getExt();

  /**
   * Read the file storage.
   *
   * @return array|string
   *   The file data.
   */
  public function read();

}
