<?php

namespace Drupal\nastorybook;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\CachedStorage;
use Drupal\Core\Render\RendererInterface as CoreRenderer;

/**
 * @todo Add class description.
 */
final class Renderer implements RendererInterface {

  /**
   * Cache id.
   */
  const CACHE_ID = 'cache_sb_components_data';

  /**
   * Template manager service.
   *
   * @var \Drupal\nastorybook\TemplateManager
   */
  private $templateManager;

  /**
   * Assets resolver service.
   *
   * @var \Drupal\nastorybook\AssetsResolver
   */
  private $assetsResolver;

  /**
   * Drupal renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * Cached storage service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cache;

  /**
   * Template data.
   *
   * @var array
   */
  private $data;

  /**
   * Template type.
   *
   * @var string
   */
  private $templateType;

  /**
   * Constructs a Renderer object.
   *
   * @param \Drupal\nastorybook\TemplateManager $templateManager
   *   Template manager service.
   * @param \Drupal\nastorybook\AssetsResolver $assetsResolver
   *   Assets resolver service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Drupal renderer service.
   * @param  \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Data cache storage.
   */
  public function __construct(
    TemplateManager $templateManager,
    AssetsResolver $assetsResolver,
    CoreRenderer $renderer,
    CacheBackendInterface $cache
  ) {
    $this->assetsResolver = $assetsResolver;
    $this->templateManager = $templateManager;
    $this->renderer = $renderer;
    $this->cache = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public function readData($path, $id = '') {
    if (!$path) {
      return [];
    }
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $path = $path[0] === DIRECTORY_SEPARATOR ? substr($path, 1) : $path;
    $path = $file_system->realpath($path);
    if (!$path) {
      return [];
    }
    if (!file_exists($path)) {
      $message = 'File %path does not exist';
      $args = ['%path' => $path];
      throw new \Exception(t($message, $args));
    }
    $json_data = file_get_contents($path);
    $data = json_decode($json_data, TRUE);
    if (json_last_error()) {
      $message = 'Error decoding JSON data in %path: %error';
      $args = ['%path' => $path, '%error' => json_last_error_msg()];
      throw new \Exception(t($message, $args));
    }
    $this->addData($data);
    return $id ? ($this->data[$id] ?? []) : $data;
  }

  /**
   * {@inheritdoc}
   */
  public function addData(array $data) {
    $cache_data = $this->cache->get(static::CACHE_ID);
    $new_data = [];
    if (!$cache_data || !$cache_data->valid) {
      $new_data = $cache_data->data ?? [];
    }
    $new_data = array_merge($new_data, $data);
    $this->cache->set(static::CACHE_ID, $new_data);
    $this->data = $new_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getData($id) {
    $data = $this->cache->get(static::CACHE_ID);
    if (!$data || !$data->data[$id]) {
      $request = \Drupal::request();
      $data = $request->getContent();
      $data = json_decode($data, TRUE) ?? [];
      $path = $data['_data'] ?? '';
      return $this->readData($path, $id);
    }
    return $data->data[$id];
  }

  /**
   * {@inheritdoc}
   */
  public function buildTemplate($name, $plugin_id = '') {
    /** @var \Drupal\nastorybook\TemplateInterface $plugin */
    $configuration = [
      '_ID' => $name,
      '_data' => $this->getData($name),
    ];
    if (empty($plugin_id) && !empty($this->templateType)) {
      $plugin_id = $this->templateType;
    }
    /** @var \Drupal\nastorybook\TemplateInterface $plugin */
    $plugin = $this->templateManager->createInstance($plugin_id, $configuration);
    return $plugin->build();
  }

  /**
   * {@inheritdoc}
   */
  public function render(array $build) {
    $result = $this->renderer->render($build);
    return $result instanceof MarkupInterface ? $result->jsonSerialize() : $result;
  }

  /**
   * {@inheritdoc}
   */
  public function renderTemplate($name, $plugin_id = '') {
    $build = $this->buildTemplate($name, $plugin_id);
    return $this->render($build);
  }

  public function setParameters(array $parameters) {
    foreach ($parameters as $key => $value) {
      switch ($key) {
        case '_data':
          $this->readData($value);
          break;

        case 'templateType':
          $this->templateType = $value;
          break;
        case '_libraries':
          break;
        default:
          $this->data[$key] = $value;
      }
    }
  }

}
