<?php

namespace Drupal\nastorybook;

/**
 * Interface for template_manager plugins.
 */
interface TemplateInterface {

  /**
   * Determine if template is existed.
   *
   * @param string $name
   *   Template name.
   *
   * @return bool
   *   The template existing status.
   */
  public static function hasTemplate($name);

  /**
   * Get build array according configuration.
   *
   * @return array
   *   Build array data.
   */
  public function build();

  /**
   * Get the template ID.
   *
   * @return string
   *   The template id.
   */
  public function getId();

  /**
   * Get all template parameters.
   *
   * @return array
   */
  public function getParameters();

  /**
   * Get the parameter value.
   *
   * @param $name
   *   The parameter name.
   * @param null $default
   *   The default value for parameter.
   *
   * @return mixed
   *   The parameter value.
   */
  public function getParameter($name, $default = NULL);

}
