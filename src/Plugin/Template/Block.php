<?php

namespace Drupal\nastorybook\Plugin\Template;

use Drupal\block\BlockViewBuilder;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\nastorybook\TemplatePluginBase;

/**
 * Plugin implementation of the template_manager.
 *
 * @Template(
 *   id = "block",
 * )
 */
class Block extends TemplatePluginBase implements TrustedCallbackInterface {

  /**
   * Instance object.
   *
   * @var array
   */
  protected static $instance = [];

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    static::$instance[$this->getId()] = $this;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $register = static::getRegistry();
    /** @var \Drupal\block\Entity\Block $block */
    $id = '_' .  $this->getId();
    $block = \Drupal\block\Entity\Block::load($id);
    if (!$block) {
      $block = \Drupal\block\Entity\Block::create(['id' => $id, 'plugin' => $this->getId()]);
      $block->save();
    }
    $build = BlockViewBuilder::lazyBuilder($id, 'full');
    $build['#pre_render'] = ($build['#pre_render'] ?? []) + [ 99 => static::class . '::setBlockData'];
    $block->delete();
    return $build;
  }

  /**
   * Get hendler service.
   *
   * @return \Drupal\Core\Block\BlockManagerInterface
   *   block manager service.
   */
  public static function getRegistry() {
    return \Drupal::service('plugin.manager.block');
  }

  public static function getInstance($id) {
    return static::$instance[$id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function hasTemplate($name) {
    return static::getRegistry()->hasDefinition($name);
  }

  public static function setBlockData($element) {
    /** @var \Drupal\nastorybook\Plugin\Template\Block $instance */
    $instance = static::getInstance($element['#plugin_id']);
    if ($instance) {
      $instance->setData($element);
    }
    return $element;
  }

  protected function setData(array &$build) {
    $childrens = \Drupal\Core\Render\Element::children($build);
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['setBlockData'];
  }

}
