<?php

namespace Drupal\nastorybook\Plugin\Template;

use Drupal\Core\Form\FormState;
use Drupal\nastorybook\TemplatePluginBase;

/**
 * Plugin implementation of the template_manager.
 *
 * @Template(
 *   id = "element",
 * )
 */
class Element extends TemplatePluginBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $this->setData($build);
    $form = $this->setFormWrapper($build);
    return $form;
  }

  /**
   * Get hendler service.
   *
   * @return \Drupal\Core\Render\ElementInfoManager
   *   Themes registry service.
   */
  public static function getRegistry() {
    return \Drupal::service('plugin.manager.element_info');
  }

  /**
   * {@inheritdoc}
   */
  public static function hasTemplate($name) {
    return static::getRegistry()->hasDefinition($name);
  }

  /**
   * {@inheritdoc}
   */
  protected function setData(array &$build) {
    $register = static::getRegistry();
    $build = $register->getInfo($this->getId());
    $variables = $this->getParameters();

    foreach ($variables as $key => $default) {
      $build['#' . $key] = $this->getParameter($key, $default);
    }
  }

  /**
   * Get element wrapper.
   *
   * @param array $build
   *   Element builder data.
   *
   * @return array
   *   Element wrapper data.
   */
  protected function setFormWrapper(array $build) {
    $register = static::getRegistry();
    $formBuilder = \Drupal::formBuilder();
    $form_id = 'form_' . $this->getId();

    $request = \Drupal::requestStack()->getCurrentRequest();
    $form_state = new FormState();
    $form_state->addBuildInfo('form_id', $form_id);
    $form = $register->getInfo('form');
    $form['#form_id'] = $form_id;
    $form['form_element__' . $this->getId()] = $build;
    $form_state->setRequestMethod($request->getMethod());
    $form_state->setSubmitted();
    $form_state->setValidationEnforced();
    $form_state->clearErrors();
    $formBuilder->prepareForm($form_id, $form, $form_state);
    $formBuilder->processForm($form_id, $form, $form_state);
    return $form;
  }

}
