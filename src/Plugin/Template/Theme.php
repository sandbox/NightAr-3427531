<?php

namespace Drupal\nastorybook\Plugin\Template;

use Drupal\nastorybook\TemplatePluginBase;

/**
 * Plugin implementation of the template_manager.
 *
 * @Template(
 *   id = "theme",
 * )
 */
class Theme extends TemplatePluginBase {

  /**
   * Get hendler service.
   *
   * @return \Drupal\Core\Theme\Registry
   *   Themes registry service.
   */
  public static function getRegistry() {
    return \Drupal::service('theme.registry');
  }

  /**
   * {@inheritdoc}
   */
  public static function hasTemplate($name) {
    return static::getRegistry()->getRuntime()->has($name);
  }

  /**
   * {@inheritdoc}
   */
  protected function setData(array &$build) {
    $register = static::getRegistry();
    $info = $register->getRuntime()->get($this->getId());
    $variables = $info['variables'] ?? [];

    foreach ($variables as $key => $default) {
      $build['#' . $key] = $this->getParameter($key, $default);
    }
  }

}
