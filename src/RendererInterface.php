<?php

namespace Drupal\nastorybook;

/**
 * @todo Add interface description.
 */
interface RendererInterface {

  /**
   * Read data from file.
   *
   * @param string $path
   *   Path to data.
   * @param string $id
   *   id of data block.
   *
   * @return array|null
   *   Array of data.
   */
  public function readData($path, $id = '');

  /**
   * Add template data to storage.
   *
   * @param array $data
   *   Template data.
   */
  public function addData(array $data);

  /**
   * Get template data by it story id.
   *
   * @param $id
   *   Story id.
   *
   * @return array|null
   */
  public function getData($id);

  /**
   * Set rendered parameters.
   *
   * @param array $parameters
   *   Rendered parameters.
   */
  public function setParameters(array $parameters);

  /**
   * Get template build data.
   *
   * @param string $name
   *   Template name.
   * @param string $plugin_id
   *   Template plugin id.
   *
   * @return array
   *   Build data.
   */
  public function buildTemplate($name, $plugin_id = '');

  /**
   * Get rendered string of build data.
   *
   * @param array $build
   *   Build data.
   *
   * @return string
   *   Rendered string of build.
   */
  public function render(array $build);

  /**
   * Get rendered string of template.
   *
   * @param string $name
   *   Template name.
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return string
   *   Rendered string.
   */
  public function renderTemplate($name, $plugin_id = '');

}
