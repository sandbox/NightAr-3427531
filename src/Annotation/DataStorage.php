<?php

namespace Drupal\nastorybook\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines template_manager annotation object.
 *
 * @Annotation
 */
final class DataStorage extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The storage file extension.
   *
   * @var string
   */
  public $ext = '';

}
