<?php

namespace Drupal\nastorybook\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines template_manager annotation object.
 *
 * @Annotation
 */
final class Template extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin weight when the type searched.
   *
   * @var string
   */
  public $weight = '100';

}
