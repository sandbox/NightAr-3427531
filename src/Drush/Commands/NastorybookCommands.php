<?php

namespace Drupal\nastorybook\Drush\Commands;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\nastorybook\ServerInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 */
final class NastorybookCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The server service.
   *
   * @var \Drupal\nastorybook\ServerInterface
   */
  private $server;

  /**
   * Constructs a NastorybookCommands object.
   */
  public function __construct(ServerInterface $server) {
    $this->server = $server;

    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nastorybook.server'),
    );
  }

  /**
   * Install the storybook server.
   */
  #[CLI\Command(name: 'nastorybook:install-server', aliases: ['ns:i'])]
  #[CLI\Argument(name: 'force', description: 'Determine if server should be installed forcibly')]
  #[CLI\Usage(name: 'nastorybook:install-server', description: 'Install the storybook server.')]
  public function installServer($force = FALSE) {
    if (!$this->server->isInstalled() || $force) {
      $this->logger()->notice($this->t('Start to install server.'));
      $this->server->install();
      $this->logger()->notice($this->t('Server is installed.'));
    }
    else {
      $this->logger()->notice($this->t('Server is already installed before.'));
    }
  }

  /**
   * Run the storybook  dev server.
   */
  #[CLI\Command(name: 'nastorybook:run-server', aliases: ['ns:r'])]
  #[CLI\Usage(name: 'nastorybook:run-server', description: 'Run the storybook server.')]
  public function runServer() {
    if ($this->server->isInstalled()) {
      $this->logger()->notice($this->t('Start to run server.'));
      $this->server->start();
      $this->logger()->notice($this->t('Server is started.'));
    }
    else {
      $this->logger()->notice($this->t('Server is not installed yet.'));
    }
  }

  /**
   * Run the storybook  dev server.
   */
  #[CLI\Command(name: 'nastorybook:build-server', aliases: ['ns:b'])]
  #[CLI\Usage(name: 'nastorybook:build-server', description: 'Run the storybook server.')]
  public function buildServer() {
    if ($this->server->isInstalled()) {
      $this->logger()->notice($this->t('Start ot build server.'));
      $this->server->build();
      $this->logger()->notice($this->t('Server is build.'));
    }
    else {
      $this->logger()->notice($this->t('Server is not installed yet.'));
    }
  }

}
