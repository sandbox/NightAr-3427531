<?php

namespace Drupal\nastorybook;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\nastorybook\Annotation\Template;

/**
 * TemplateManager plugin manager.
 */
final class TemplateManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Template', $namespaces, $module_handler, TemplateInterface::class, Template::class);
    $this->alterInfo('template_manager_info');
    $this->setCacheBackend($cache_backend, 'template_manager_plugins');
  }

  public function createInstance($plugin_id, array $configuration = []) {
    if (!$this->hasDefinition($plugin_id)) {
      $plugin_id = $this->getFallbackPluginId($plugin_id, $configuration);
    }
    $definition = $this->getDefinition($plugin_id);
    $template_id = $configuration['_ID'] ?? '';
    if (!$definition['class']::hasTemplate($template_id)) {
      throw new PluginException(sprintf('Plugin (%s) template "%s" is not exist.', $plugin_id, $template_id));
    }
    return parent::createInstance($plugin_id, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    $definitions = $this->getDefinitions();
    $template_id = $configuration['_ID'] ?? '';
    foreach ($definitions as $definition) {
      if ($definition['class']::hasTemplate($template_id)) {
        return $definition['id'];
      }
    }
    if (!$this->hasDefinition($plugin_id)) {
      throw new PluginNotFoundException($plugin_id, sprintf('The "%s" plugin does not exist.', $plugin_id));
    }
  }

}
