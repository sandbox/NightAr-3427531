<?php

namespace Drupal\nastorybook;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;

final class StoriesData {

  /**
   * Stories files mask.
   */
  const MASK_STORIES = DIRECTORY_SEPARATOR . '**' . DIRECTORY_SEPARATOR . '*.stories.@(json|yaml|yml)';

  /**
   * Data files mask.
   */
  const MASK_DATA = DIRECTORY_SEPARATOR . '**' . DIRECTORY_SEPARATOR . '(*.)?data.@(json|yaml|yml)';

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Module list extension.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private $moduleList;

  /**
   * Theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  private $themeHandler;

  /**
   * Constructs a Storybook object.
   */
  public function __construct(FileSystemInterface $fileSystem, ModuleExtensionList $moduleList, ThemeHandlerInterface $themeHandler) {
    $this->fileSystem = $fileSystem;
    $this->moduleList = $moduleList;
    $this->themeHandler = $themeHandler;
  }



  public function getDirestories($wrap_suffix = TRUE) {
    $suffix = $wrap_suffix ? static::MASK_STORIES : '';
    $directories = [];
    $theme = $this->themeHandler->getTheme($this->themeHandler->getDefault());
    $directories[] = $this->fileSystem->realpath($theme->getPath())  . DIRECTORY_SEPARATOR . 'stories' . $suffix;
    while ($theme->base_theme) {
      $theme = $this->themeHandler->getTheme($theme->base_theme);
      if ($path = $this->fileSystem->realpath($theme->getPath() . DIRECTORY_SEPARATOR . 'stories')) {
        array_unshift($directories, $path . $suffix);
      }
    }
    foreach ($this->moduleList->getList() as $module) {
      $is_core = $module->info['package']  && $module->info['package'] === 'Core';
      $path = $this->fileSystem->realpath($module->getPath() . DIRECTORY_SEPARATOR . 'stories');
      if (!$is_core && $path) {
        array_unshift($directories, $path . $suffix);
      }
    }
    if ($path = $this->fileSystem->realpath('public://storybook/stories')) {
      array_unshift($directories, $path . $suffix);
    }
    return $directories;
  }

}
