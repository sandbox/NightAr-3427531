<?php

namespace Drupal\nastorybook;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;

/**
 * @todo Storybook service.
 */
final class Server implements ServerInterface {

  /**
   * @var string Storybook path.
   */
  protected $storybookPath = 'public://storybook';

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Module list extension.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private $moduleList;

  /**
   * Constructs a Storybook object.
   */
  public function __construct(FileSystemInterface $fileSystem, ModuleExtensionList $moduleList) {
    $this->fileSystem = $fileSystem;
    $this->moduleList = $moduleList;
  }

  /**
   * {@inheritdoc}
   */
  public function install(): void {
    $this->copyServer();
    $this->installNode();
  }

  public function start() {
    exec('npm -v', $output, $exitCode);
    if ($exitCode === 0 && version_compare($output[0], '9.0.0', '>=') && $this->isInstalled()) {
      $path = $this->fileSystem->realpath($this->storybookPath);
      exec('npm run storybook --prefix ' . $path, $output, $exitCode);
    }
  }

  public function build() {
    exec('npm -v', $output, $exitCode);
    if ($exitCode === 0 && version_compare($output[0], '9.0.0', '>=') && $this->isInstalled()) {
      $path = $this->fileSystem->realpath($this->storybookPath);
      exec('npm run build-storybook --prefix ' . $path, $output, $exitCode);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isInstalled(): bool {
    $path = $this->fileSystem->realpath($this->storybookPath);
    $node_modules_path = $path . '/node_modules';
    if (!is_dir($path) || !is_dir($node_modules_path)) {
      return FALSE;
    }
    return TRUE;
  }

  public function copyServer() {
    $from_path = $this->moduleList->getPath('nastorybook') . '/server';
    $this->fileSystem->prepareDirectory($this->storybookPath, FileSystemInterface::CREATE_DIRECTORY);
    $map = $this->getDirMap($from_path);
    foreach ($map as $item) {
      $rel_path = substr($item['path'], strlen($from_path));
      $path = $this->storybookPath . $rel_path;
      if ($item['is_dir']) {
        $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
      }
      else {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        if (strtolower($ext) === 'back') {
          $path = $this->fileSystem->dirname($path) . DIRECTORY_SEPARATOR . $this->fileSystem->basename($path, '.back');
        }
        $this->fileSystem->copy($item['path'], $path, FileSystemInterface::EXISTS_REPLACE);
        $this->replacePatterns($path);
      }
    }
  }

  protected function replacePatterns($path) {
    $path = $this->fileSystem->realpath($path) ?: $path;
    $data = file_get_contents($path);
    $options = ['absolute' => TRUE];
    $count = 0;
    $data = str_replace(
      [
        '@paths-route',
        '@api-route',
      ], [
      Url::fromRoute('nastorybook.stories_paths', [], $options)->toString(),
      Url::fromRoute('nastorybook.components_api', [], $options)->toString(),
    ],
      $data, $count);
    if ($count) {
      $this->fileSystem->saveData($data, $path, FileSystemInterface::EXISTS_REPLACE);
    }
  }

  protected function getDirMap($dir, $map = []) {
    $dir_path = $this->fileSystem->realpath($dir) ?: $dir;
    $dir_data = scandir($dir_path);
    if (empty($dir_data)) {
      return $map;
    }
    foreach ($dir_data as $item) {
      if (in_array($item, ['.', '..'])) {
        continue;
      }
      $item_path = $dir . DIRECTORY_SEPARATOR . $item;
      $is_dir = is_dir($item_path);
      if ($is_dir) {
        $map = $this->getDirMap($item_path, $map);
      }
      $method = $is_dir ? 'array_unshift': 'array_push';
      $method($map, [
        'path' => $item_path,
        'rel_path' => '',
        'name' => $item,
        'is_dir' => $is_dir,
      ]);
    }
    return $map;
  }

  protected function installNode () {
    exec('npm -v', $output, $exitCode);
    if ($exitCode === 0 && version_compare($output[0], '9.0.0', '>=')) {
      $path = $this->fileSystem->realpath($this->storybookPath);
      exec('npm i --prefix ' . $path, $output, $exitCode);
    }
  }

}
