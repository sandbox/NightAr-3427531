<?php

namespace Drupal\nastorybook;

/**
 * @todo Add interface description.
 */
interface ServerInterface {

  /**
   * Install server to public folder if it is not installed yet.
   */
  public function install(): void;

  /**
   * Determine if the server is installed.
   *
   * @return bool
   *   The server installed status.
   */
  public function isInstalled(): bool;

}
